import App from '../../app';
import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = `🎊	${fighter.name} is a winner! 🎊`;
  const bodyElement = showFighter(fighter);
  showModal({
    title,
    bodyElement,
    onClose
  });
}

function showFighter(fighter) {
  const img = createFighterImage(fighter);
  const fighterElement = createElement({
    tagName: 'div',
    className: 'arena___fighter'
  });
  fighterElement.append(img);
  
  return fighterElement;
}

function onClose() {
  const arena = document.querySelector('.arena___root');
  arena.remove();
  new App();
}