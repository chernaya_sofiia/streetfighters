import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    AddEventListeners();

    function AddEventListeners() {
      document.addEventListener('keydown', KeyDown);
      document.addEventListener('keyup', KeyUp);
    }

    function RemoveEventListeners() {
      document.removeEventListener('keydown', KeyDown);
      document.removeEventListener('keyup', KeyUp);
    }

    const FighterOne = {
      properties: firstFighter,
      currentHealth: firstFighter.health,
      block: false,
      criticalHitComb: 0,
      criticalHitCombTime: Date.now() - 10000,
      indicator: document.getElementById("left-fighter-indicator")
    }

    const FighterTwo = {
      properties: secondFighter,
      currentHealth: secondFighter.health,
      block: false,
      criticalHitComb: 0,
      criticalHitCombTime: Date.now() - 10000,
      indicator: document.getElementById("right-fighter-indicator")
    }

    function KeyDown(event) {
      if (!event.repeat) {
        const code = event.code;
        if (code === controls.PlayerOneAttack) {
          if (!FighterOne.block && !FighterTwo.block) {
            DoHit(FighterOne, FighterTwo);
            showIcon(FighterOne, "hit");
          }
        }
        if (code === controls.PlayerTwoAttack) {
          if (!FighterTwo.block && !FighterOne.block) {
            DoHit(FighterTwo, FighterOne);
            showIcon(FighterTwo, "hit");
          }
        }

        if (code === controls.PlayerOneBlock) {
          FighterOne.block = true;
          showIcon(FighterOne, "defence");
        }
        if (code === controls.PlayerTwoBlock) {
          FighterTwo.block = true;
          showIcon(FighterTwo, "defence");
        }

        if (controls.PlayerOneCriticalHitCombination.includes(code)) {
          CriticalHit(FighterOne, FighterTwo);
        }
        if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
          CriticalHit(FighterTwo, FighterOne);
        }
      }
    }

    function KeyUp(event) {
      const code = event.code;
      if (code === controls.PlayerOneBlock) {
        FighterOne.block = false;
      }
      if (code === controls.PlayerTwoBlock) {
        FighterTwo.block = false;
      }

      if (controls.PlayerOneCriticalHitCombination.includes(code)) {
        FighterOne.criticalHitComb -= 1;
      }
      if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
        FighterTwo.criticalHitComb -= 1;
      }
    }

    function DoHit(attacker, defender) {
      const damage = getDamage(attacker, defender);
      defender.currentHealth = defender.currentHealth - damage;
      setIndicator(defender);
      if (defender.currentHealth <= 0) {
        RemoveEventListeners();
        resolve(defender.properties);
      }
    }

    function CriticalHit(attacker, defender) {
      attacker.criticalHitComb += 1;
      if (attacker.criticalHitComb === 3) {
        showIcon(attacker, "critical-hit");
        const curDate = Date.now();
        if (curDate - attacker.criticalHitCombTime >= 10000) {
          attacker.properties.attack = attacker.properties.attack * 2;
          DoHit(attacker, defender);
          attacker.criticalHitCombTime = curDate;
        }
        attacker.criticalHitComb = 0;
      }
    }
  });
}

function showIcon(fighter, type) {
  const iconElement = `arena-fighter___icon-${type}`;
  fighter.indicator.classList.add(iconElement);
  setTimeout(() => {
    fighter.indicator.classList.remove(iconElement);
  }, 200);
}

function setIndicator(fighter) {
  const indicatorWidth = fighter.currentHealth * 100 / fighter.properties.health;
  fighter.indicator.style.width = (indicatorWidth <= 0 ? 0 : indicatorWidth) + '%';
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;

  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
  const hitPower = fighter.properties.attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
  const blockPower = fighter.properties.defense * dodgeChance;

  return blockPower;
}
