import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const info = createFighterInfo(fighter);
    const img = createFighterImage(fighter);
    fighterElement.append(img, info);
  }
  else if (fighter === false) {
    const msg = "Fail to load a fighter";
    const error = createErrorElement(msg);
    fighterElement.append(error);
  }

  return fighterElement;
}

function createFighterInfo(fighter) {
  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });
  const name = fighter.name;
  const health = `&#128154; Health: ${fighter.health}`;
  const attack = `&#128074; Attack: ${fighter.attack}`;
  const defense = `&#128737; Defense: ${fighter.defense}`;
  const details = [name, health, attack, defense];
  for (let property of details) {
    const element = createElement({
      tagName: 'p',
      className: 'fighter-preview___info-details',
    });
    element.innerHTML = property;
    infoElement.append(element);
  }

  return infoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createErrorElement(msg) {
  const element = createElement({
    tagName: 'div',
    className: 'fighter-preview___info-error'
  });
  element.innerText = msg;

  return element;
}
